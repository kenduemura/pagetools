package com.utech.confluence.plugins.pagetools.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * A resource of message.
 */
@Path("/")
public class PagePermissionRest {

    private final UserManager userManager;
    private final UserAccessor userAccessor;
    private final PageManager pageManager;
    private final PermissionManager permissionManager;
    private final Logger log = LoggerFactory.getLogger(PagePermissionRest.class);

    public PagePermissionRest(UserManager userManager, UserAccessor userAccessor, PageManager pageManager, PermissionManager permissionManager)
    {
        this.userManager = userManager;
        this.userAccessor = userAccessor;
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUser(@Context HttpServletRequest request)
    {
        String username = userManager.getRemoteUsername(request);
        return Response.ok(new PagePermissionRestModel()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postPermission(PagePermissionRestModel pagePermissionRestModelList, @Context HttpServletRequest request)
    {

        Page page = pageManager.getPage(pagePermissionRestModelList.getPageId());

        // @TODO: For each user
        // * Get User
        // * Find if user has access
        for (PagePermissionRestModel.User user: pagePermissionRestModelList.getUsers())
        {
            User currentUser = null;
            if (user.getUsername() != null) {
                currentUser = userAccessor.getUser(user.getUsername());
                log.debug("By Username: " + currentUser.getFullName());
            } else if ( user.getEmailAddress() != null) {
                for ( User u : (List<User>) userAccessor.getUsersByEmail(user.getEmailAddress()).pager().getCurrentPage() )
                {
                    currentUser = u;
                    log.debug("By Email: " + currentUser.getFullName());
                }
            } else {
                // @TODO: Error unacceptable
                // Response.status(403).
                return Response.status(403).build();
            }

            if (currentUser == null)
            {
                // @TODO: User Not Found
                return Response.status(403).build();
            }

            user.setViewPermission(permissionManager.hasPermission(currentUser, Permission.VIEW, page));
            user.setEditPermission(permissionManager.hasPermission(currentUser, Permission.EDIT, page));
/*            user.setComment(permissionManager.hasPermission(currentUser, Permission., page));
            user.setAttachment(permissionManager.hasPermission(currentUser, Permission.VIEW, page));*/
        }

        return Response.ok(pagePermissionRestModelList).build();
    }
}