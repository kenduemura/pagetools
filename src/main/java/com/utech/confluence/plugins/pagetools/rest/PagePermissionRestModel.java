package com.utech.confluence.plugins.pagetools.rest;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PagePermissionRestModel {

    @XmlElement Integer pageId;
    @XmlElement private List<User> users;

    public static final class User
    {
        @XmlElement private String username;
        @XmlElement private String emailAddress;
        @XmlElement private boolean view;
        @XmlElement private boolean edit;
        @XmlElement private boolean comment;
        @XmlElement private boolean attachment;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmailAddress() {
            return emailAddress;
        }

        public void setEmailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
        }

        public boolean isView() {
            return view;
        }

        public void setViewPermission(boolean view) {
            this.view = view;
        }

        public boolean isEdit() {
            return edit;
        }

        public void setEditPermission(boolean edit) {
            this.edit = edit;
        }

        public boolean isComment() {
            return comment;
        }

        public void setComment(boolean comment) {
            this.comment = comment;
        }

        public boolean isAttachment() {
            return attachment;
        }

        public void setAttachment(boolean attachment) {
            this.attachment = attachment;
        }

    }

    public Integer getPageId() {
        return pageId;
    }

    public List<User> getUsers() {
        return users;
    }

}