package ut.com.utech.confluence.plugins.pagetools;

import org.junit.Test;
import com.utech.confluence.plugins.pagetools.MyPluginComponent;
import com.utech.confluence.plugins.pagetools.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}